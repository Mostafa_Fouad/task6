/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task_5;

import java.util.Scanner;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author MostafaFouad
 * section 2
 * B.N. 18
 */

public class Task_5 {

//declaring some parameters
    
     static Scanner  typeInput = new Scanner(System.in);
     static Scanner  input = new Scanner(System.in);
     static Scanner IDinput = new Scanner(System.in);
     static Scanner slotsinput = new Scanner(System.in);
     static int numberOfSlots;
     private  static String carName;
     private  static String carID;
     private  static String carStartTime;
     private  static String key;
     private  static int counter=0;
     static String [] carList;
     static String addCar;
     
    public static void main(String[] args) throws ParseException {
           
        //asking user to declare number of slots of the parking
             System.out.print("Enter Parking capacity slots:"+" ");
             numberOfSlots=slotsinput.nextInt();
             
             callOthers();//call a rercursive function to implement the code
    }
    
    
      static void callOthers() throws ParseException{
         String [] str1 ;
         String s="";
         Date date = new Date();
         String strDateFormat = "hh:mm:ss";
         DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
         String formattedDate= dateFormat.format(date);
        carStartTime=formattedDate;
        
                    System.out.println("\n"+
                                        "1-Enter new car to the parking (Press 1)\n"+
                                        "2-Remove a car from the parking(Press 2)\n"+
                                        "3-Print all the cars in the parking (press 3)\n"+
                                        "4-End (press 4)"
                                      );
    
                     key=input.nextLine();
                     
                     if(key.equals("1")||key.equals("2")||key.equals("3")||key.equals("4")){
                     
                        
             switch(key){
             
                 case "1": // to add cars
                     
                    if(!isFull(Cars.newCar.size(),numberOfSlots)){
                        
                            System.out.print("Enter car type :"+" ");
                            carName=typeInput.nextLine();
                            System.out.print("Enter car ID in form of (#+number or letter):"+" ");
                            carID=IDinput.nextLine();
                            char id=carID.charAt(0);
                            
                        if(id=='#'){//check for the format of the id is written in right way before adding the new car
                            
                            if (Cars.newCar.isEmpty()){
                                        Cars.newCar.add(new Cars(carName,carID,carStartTime));
                                        System.out.println("\nThere is "+Cars.newCar.size()+" car in the parking");
                            }
                            
                            else
                            {
                                for(int i=0;i<Cars.newCar.size();i++){
                                 s=Cars.newCar.get(i).toString();
                                 str1=s.split("		", 3);
                                 s=str1[1];
                                 
                                }
                                 if(s.compareTo(carID)==0){ //check if there is another car has the same id before park a new car
                                 
                                 System.out.println("There is another car has the same ID, please enter your car again with"
                                         + " different ID");
                                 }
                                 else
                                 { Cars.newCar.add(new Cars(carName,carID,carStartTime));
                              System.out.println("\nThere are "+Cars.newCar.size()+" cars in the parking");
                                 }
                        
                             }
                        }
                                
                         else
                                System.out.println("ID entered format is wrong please enter your car again and car id in the right format");   
                    }
                     else       
                            System.out.println("Parking is full !!");
                    
                 break;
                   
                 case "2": // to remove cars
                     
                         if(Cars.newCar.isEmpty()){
                          
                          System.out.println("Parking is empty");
                           }
                     
                              
                       else{
                            String string="";
                            int i=0;
                            System.out.print("Enter car ID:"+" ");
                             carID=IDinput.nextLine();
                             
                            for( i=0 ;i<Cars.newCar.size();i++){

                                    string = Cars.newCar.get(i).toString();
                                    
                                    if(!string.contains(carID)){
                                        
                                        if(i==Cars.newCar.size()-1){
                                   System.out.println("The id you entered is not in the parking !, enter a right id.");
                                        callOthers();
                                        }
                                        
                                    }
                             
                                    else {
                                      
                                        for( i=0 ;i<Cars.newCar.size();i++){

                                            String removeCar = Cars.newCar.get(i).toString();

                                            if(removeCar.contains(carID))
                                            {  

                                               String [] str;
                                               String leftTime;
                                               String leftTime1;
                                               leftTime=Cars.newCar.get(i).toString();
                                               leftTime1=leftTime;
                                               str=leftTime.split("		", 3);
                                               leftTime=str[2];
                                               //System.out.println(leftTime);
                                               String t1 = leftTime;
                                               String t2 = formattedDate;
                                               DateFormat df = new java.text.SimpleDateFormat("hh:mm:ss");
                                               Date date1 = df.parse(t1);
                                               Date date2 = df.parse(t2);
                                               long difference = date2.getTime() - date1.getTime();
                                               Cars.newCar.remove(i);

                                               //print the ticket 
                                               System.out.println("CarType"+"\t\t"+"CarID"+"\t\t"+"StartTime"+"\t\t"+"leftTime"+"\t\t"+"duration in the parking"+"\t\t"+"\n");
                                               System.out.println(leftTime1+"\t\t"+formattedDate+"\t\t"+(difference/(1000*60))+" minute(s)");
                                                i=0;
                                              callOthers();
                                            }
                                            
                                            else
                                                i=0;
                                                
                                                
                                         }
                                      }
                                   }
                                }
                         
                    break;
                    
                 case "3": // to print all cars in the parking
                     
                                if(Cars.newCar.isEmpty())
                                {System.out.println("The parking is empty!");}

                                else {
                                        System.out.println("CarType"+"\t\t"+"CarID"+"\t\t"+"StartTime:\n");
                                       
                                        for(int i=0;i<Cars.newCar.size();i++)
                                        System.out.println(Cars.newCar.get(i));  
                                }
                 break;
                      
                 case "4": // to end the code
                     return;
                           
             }
        }
                     else 
                         
                     {System.out.println("please enter 1,2,3 or 4");}
                     
             callOthers();
                   
      }
    
          
   static boolean isFull(int x,int y){ // check if the parking is not full to add new car
       
       if(x<y)
                {return false;}
    
       else 
        
                {return true;}
   
    }

}